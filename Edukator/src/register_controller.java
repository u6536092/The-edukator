

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mypack.dao;

/**
 * Servlet implementation class reg
 */
public class register_controller extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public register_controller() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	

		String fname=request.getParameter("fname");
		String lname=request.getParameter("lname");
		String email=request.getParameter("email");
		String username=request.getParameter("username");
		String password=request.getParameter("pass");
		String mobile=request.getParameter("mobile");
	    String usertype="user";
	
	  Connection connection=dao.getConnection();
	  PreparedStatement statement;
	  
	  String insert_query="insert into login values(?,?,?,?,?,?,?)";

	  try
	  {
	  statement = connection.prepareStatement(insert_query);
	
	  
	  statement.setString(1,username);
	  statement.setString(2,password);
	  statement.setString(3,fname);
	  statement.setString(4,lname);
	  statement.setString(5,email);
	  statement.setString(6,mobile);
	  statement.setString(7,usertype);
	  statement.executeUpdate();


	  response.sendRedirect("login.jsp");

	  
	  }
	  catch (Exception e)
	  {
		  e.printStackTrace();
	  }
	
	}

}
