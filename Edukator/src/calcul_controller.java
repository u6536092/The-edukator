

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class calcul_controller
 */
public class calcul_controller extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public calcul_controller() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
	int x= Integer.parseInt(request.getParameter("x"));
	

	int y= Integer.parseInt(request.getParameter("y"));
	
	String op=request.getParameter("op");
	
	double result;
	
	PrintWriter p=response.getWriter();
	
	
	if(op.equals("add"))
	{
		result=x+y;
		p.print(result);
	}
	

	else if(op.equals("sub"))
	{
		result=x-y;
		p.print(result);
	}
	

	else if(op.equals("mul"))
	{
		result=x*y;
		p.print(result);
	}

	else if(op.equals("div"))
	{
		result=x/y;
		p.print(result);
	}
	
	
	
	}

}
