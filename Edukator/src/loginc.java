

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mypack.dao;

/**
 * Servlet implementation class loginc
 */
public class loginc extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public loginc() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException  {
		// TODO Auto-generated method stub
	
	
	


Connection connection=dao.getConnection();
PreparedStatement statement;
ResultSet set;
RequestDispatcher dispatcher;

String sql="select * from login where username=?";


String username=request.getParameter("username");
String password=request.getParameter("password");
String usertype=request.getParameter("usertype");

try{

	String username_database=null;
	String password_database=null;
    String usertype_database=null;
	
	
statement=connection.prepareStatement(sql);
statement.setString(1,username);
set=statement.executeQuery();

PrintWriter p=response.getWriter();



if(set.next())
	
{
	username_database=set.getString(1);
	password_database=set.getString(2);
	usertype_database=set.getString(7);
}

if(usertype.equals("user"))
{
	

if(username.equalsIgnoreCase(username_database) && password.equals(password_database) && usertype.equals(usertype_database) )

{
	
	
	request.setAttribute("username", username);
	dispatcher=request.getRequestDispatcher("front.jsp");
	dispatcher.forward(request, response);
	p.print("nice work");
}

else
{
	p.print("Login failed");
}




}

else if(usertype.equals("admin")) 	
	
	{
	if(username.equalsIgnoreCase(username_database) && password.equals(password_database) && usertype.equals(usertype_database) )

	{

	 response.sendRedirect("admin_op.jsp");
	
	
	}
	
	else
	{
		p.print("Login failed");
	}


}



}


catch(Exception e)
{
	e.printStackTrace();
}
}


}
