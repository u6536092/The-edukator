

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class convertor_controller
 */
public class convertor_controller extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public convertor_controller() {
        super();
        // TODO Auto-generated constructor stub
    }
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

	double a=Double.parseDouble(request.getParameter("a"));

	String to=request.getParameter("to");

	String from=request.getParameter("from");
	
	
	PrintWriter p=response.getWriter();
	
	double result;
	
	
	if(from.equals("meter") && to.equals("inch"))
	{
		result=a*39.37;
		p.print("The result is " + result);
		
	}
	
	else if(from.equals("meter")  && to.equals("foot"))
	{
		result=a*3.28084;
		p.print("The result is " + result);
		
	}

	else if(from.equals("meter")  && to.equals("yard"))
	{
		result=a*1.09361;
		p.print("The result is " + result);
		
	}
	else if(from.equals("meter")  && to.equals("mile"))
	{
		result=a*0.000621371;
		p.print("The result is " + result);
		
	}
	

	else if(from.equals("inch")  && to.equals("meter"))
	{
		result=a* 0.0254;
		p.print("The result is " + result);
		
	}
	

	else if(from.equals("inch")  && to.equals("foot"))
	{
		result=a*0.08333333;
		p.print("The result is " + result);
		
	}

	else if(from.equals("inch")  && to.equals("yard"))
	{
		result=a*0.02777778;
		p.print("The result is " + result);
		
	}

	else if(from.equals("inch")  && to.equals("mile"))
	{
		result=a*( 0.0015782830000000002/100);
		p.print("The result is " + result);
		
	}
	

	else if(from.equals("foot")  && to.equals("meter"))
	{
		result=a*0.3048;
		p.print("The result is " + result);
		
	}

	else if(from.equals("foot")  && to.equals("inch"))
	{
		result=a*0.12;
		p.print("The result is " + result);
		
	}

	else if(from.equals("foot")  && to.equals("yard"))
	{
		result=a*0.33333333;
		p.print("The result is " + result);
		
	}

	else if(from.equals("foot")  && to.equals("mile"))
	{
		result=a*(0.001893939/10);
		p.print("The result is " + result);
		
	}

	else if(from.equals("yard")  && to.equals("meter"))
	{
		result=a*0.9144;
		p.print("The result is " + result);
		
	}

	else if(from.equals("yard")  && to.equals("inch"))
	{
		result=a*36;
		p.print("The result is " + result);
		
	}

	else if(from.equals("yard")  && to.equals("foot"))
	{
		result=a*3;
		p.print("The result is " + result);
		
	}

	else if(from.equals("yard")  && to.equals("mile"))
	{
		result=a*0.005681818;
		p.print("The result is " + result);
		
	}

	else if(from.equals("mile")  && to.equals("meter"))
	{
		result=a*1609.344;
		p.print("The result is " + result);
		
	}

	else if(from.equals("mile")  && to.equals("foot"))
	{
		result=(a*10)*526;
		p.print("The result is " + result);
		
	}

	else if(from.equals("mile")  && to.equals("inch"))
	{
		result=(a*10)*6336;
		p.print("The result is " + result);
		
	}

	else if(from.equals("mile")  && to.equals("yard"))
	{
		result=(a*10)*176;
		p.print("The result is " + result);
			
	}

	
	
	
	}

}
